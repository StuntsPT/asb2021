# Assignment 02

## *High Throughput Sequencing at your fingertips*


### Deadline:

June 25th 2021


### Delivery format:

* English or Portuguese
* PDF


### Delivery method:

E-mail


### The data:

For this assignment you will be provided with [a pre-built High Throughput Sequencing (HTS) dataset](https://gitlab.com/StuntsPT/small_test_dataset/-/tree/master/Cornales).
Along with the data you will find a small `.md` (Markdown) file with important information regarding the data.


### Backstory

* Explorers have found a plant type, deep in the Amazonian forest, with several unique characteristics. Among these plants, a few individuals have particularly smelly flowers (named *Pungent*), whereas most individuals have no noticeable odour (named *Bland*). Some claim that this is due to *Pungent* individuals belonging to a different species, which just happen to look the same as *Bland* individuals, whereas other claim this is just likely to be due to intra-specific variability.
* Sequencing several plastidial and mitochondrial genes using Sanger technology, was inconclusive regarding the above problem, since the number of differences between *Pungent* and *Bland* individuals was larger than between *Bland* individuals, but not by any significant margin.
* As such, *Pungent* and *Bland* individuals were sequenced using RAD-Seq methods.


### Your Task:

* Figure out if the data has the resolution to distinguish between *Pungent* and *Bland* individuals.
* What groups can you identify?
* Write a report on what you did to try to answer that question;
* Keep in mind that your are not analysing a full dataset, but rather a heavily down-sampled subset.


### In detail:

* Write an *introduction* section describing the sequencing method you are working with;
  * Highlight the importance of HTS data on being able to perform the task you are resolving;
* Make sure you have a *Materials & Methods* section where you **detail** how your analyses were performed;
  * Do not forget to describe the dataset as best you can!
* Include a *results* section where you describe the results you have obtained;
* Interpret the results in a biological context in the *Discussion* section;
* Optionally, finish with a *conclusion* section if you think it makes sense in your specific case;


### Hints:

* Make your analyses **reproducible** (detail them as much as you can, so that you can fully repeat them 5 years later);
* Include any commands you use;
* If you wrote any scripts, include them as appendices (or even better place them in an online repository like [github](https://github.com) or [gitlab](https://gitlab.com));
* This dataset might take several hours to analyse (depending mostly on CPU speed). Plan in advance;
* DO NOT FORGET TO INCLUDE REFERENCES!! These have, of course, to be included in the main text, like you see in every research paper;
 * When making citations for software, cite the respective paper (when available) instead of the website;
* This analysis is not as straightforward as the one from the tutorial. **There are bound to be problems and other analyses issues**. Solve them as best you can;
* There are tenths (hundreds?) of different ways to solve this problem. Solutions that are too similar between groups will be penalized (all groups with similar solutions will be penalized);
* Do not hesitate to ask any questions you may have via email or Discord;


### Notes:

* Original data this mockup was derived from can be found in the [dryad repository](https://doi.org/10.5061/dryad.bb3h52t) (It was heavily modified, though!) 
* Original research paper: [Resolving relationships and phylogeographic history of the Nyssa sylvatica complex using data from RAD-seq and species distribution modeling](https://doi.org/10.1016/j.ympev.2018.04.001)
